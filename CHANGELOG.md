# CHANGELOG.md

## Unreleased

Changes:

- Drop Python 3.6 and 3.7

## 1.2.0

New features:

- The `BESDB` parameters "initiator", "host" and "port" are now optional and have defaults.

## 1.1.0

New features:

- When `request_id` is not a valid `bson` ObjectId, use a hash of `request_id` to obtain a valid ObjectId.

## 1.0.0

## 0.7.0

New features:

- Add `db_options` argument to `Workflow` constructor.

Changes:

- Deprecate `BESDB` environment variables.
- Deprecate `pypushflow.logutils.basicConfig`.
- Keep mongo client connected only during execution.

## 0.6.2

Bug fixes:

- Sanitize data before sending to mongo.

## 0.6.1

Changes:

- PythonActor: allow downstream data to be configured.

## 0.6.0

Changes:

- A worflow that fails without raising an exception returns the exception instance.

## 0.5.0

Changes:

- BES database refactoring.

## 0.4.0

New features:

- Support different execution pools
  - thread
  - gevent
  - process (non-daemonic processes)
  - ndprocess (non-daemonic processes)
  - multiprocessing (daemonic processes)
  - ndmultiprocessing (non-daemonic processes)
  - billiard (daemonic processes)
  - scaling (scaling workers)
- Stop workflows with and without interrupting running tasks.
- Stop workflows on certain signals.

## 0.3.0

First version used by Ewoks.

Changes:

- Workflow stops when all execution threads finished
  (before it relied on triggering the stop actor).
- Refactor logging.

## 0.2.0 (unreleased)

Obsolete attempt at using PyPushflow in Orange3 workflow projects.

## 0.1.0

Original version used at ESRF MX beamlines.
